FROM openjdk:8

RUN apt update; apt install -y wget \
                               unzip\
                               curl \
                               git \
                               vim \
                               g++

WORKDIR /root

#RUN wget http://nlp.stanford.edu/software/stanford-corenlp-full-2016-10-31.zip
RUN wget http://nlp.stanford.edu/software/stanford-corenlp-full-2016-10-31.zip -P /root/data-storage

RUN cd /root/data-storage && unzip stanford-corenlp-full-2016-10-31.zip

WORKDIR /root/data-storage/stanford-corenlp-full-2016-10-31

ENTRYPOINT ["java", "-mx4g", "-cp", "*", "edu.stanford.nlp.pipeline.StanfordCoreNLPServer", "-port", "9020"]
